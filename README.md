
Currently uses libusb-1.0.9, which was obtained from [http://libusb.org].

## Build

1. Edit the makefile to indicate where your android tools are.
1. Run: `make`

## Run

1. Download it: `adb push libs/armeabi-v7a/listdevs /data`
1. Run it: `adb shell /data/listdevs`



