LOCAL_PATH := $(call my-dir)
LIBUSB := $(LIBUSB_SRC)

#
# Make the library
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(LIBUSB)/libusb/core.c
LOCAL_SRC_FILES += $(LIBUSB)/libusb/descriptor.c
LOCAL_SRC_FILES += $(LIBUSB)/libusb/io.c
LOCAL_SRC_FILES += $(LIBUSB)/libusb/sync.c
LOCAL_SRC_FILES += $(LIBUSB)/libusb/os/linux_usbfs.c 
LOCAL_SRC_FILES += $(LIBUSB)/libusb/os/threads_posix.c

LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(LIBUSB)/libusb
LOCAL_C_INCLUDES += $(LIBUSB)/libusb/os

LOCAL_CFLAGS := -DLIBUSB_DESCRIBE=\"libusb-1.0.9-android\"

LOCAL_EXPORT_C_INCLUDES := $(LIBUSB)/libusb

LOCAL_LD_LIBS := -llog

LOCAL_MODULE := libusb1.0

LOCAL_RELINK_MODULE := false

include $(BUILD_STATIC_LIBRARY)


#
# make lsusb to test
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(LIBUSB)/examples/listdevs.c
LOCAL_LDFLAGS := -llog
LOCAL_STATIC_LIBRARIES := libusb1.0
LOCAL_MODULE := listdevs

include $(BUILD_EXECUTABLE)

