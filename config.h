/* just guessing */

#undef ENABLE_DEBUG_LOGGING

#define ENABLE_LOGGING
#define HAVE_DLFCN_H 1
#define HAVE_GETTIMEOFDAY 1
#define HAVE_INTTYPES_H 1
#define OS_LINUX 1
#define USE_SYSTEM_LOGGING_FACILITY 1
#define POLL_NFDS_TYPE nfds_t
#define THREADS_POSIX 1
#define DEFAULT_VISIBILITY __attribute__((visibility("default")))
#define HAVE_MEMORY_H 1
#define HAVE_POLL_H 1
#define HAVE_SIGNAL_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_UNISTD_H 1
#define HAVE_LINUX_FILTER_H 1
#define HAVE_LINUX_NETLINK_H 1
#define HAVE_ASM_TYPES_H 1
#define HAVE_SYS_SOCKET_H 1

#define LIBUSB_DESCRIBE "libusb-1.0.9-android"

/* android does not provide this */
# define TIMESPEC_TO_TIMEVAL(tv, ts) {                                   \
	(tv)->tv_sec = (ts)->tv_sec;                                    \
	(tv)->tv_usec = (ts)->tv_nsec / 1000;                           \
}


