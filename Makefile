
# update this
ANDROID_TOOLS=/home/jimix/work/mare/cross/cross-build/android-tools

# you can grab libusb from this place
LIBUSB := libusb-1.0.9

# use the default installed ndk
ANDROID_VERSION=android-ndk-google

# newer compilers are awesome!

MY_NDK_TOOLCHAIN_VERSION:=4.8

MY_BUILD=$(ANDROID_TOOLS)/$(ANDROID_VERSION)/ndk-build

MY_OPTS := NDK_TOOLCHAIN_VERSION:=$(MY_NDK_TOOLCHAIN_VERSION)
MY_OPTS += APP_STL:=gnustl_static
MY_OPTS += APP_ABI=armeabi-v7a
MY_OPTS += LIBUSB:=$(abspath $(LIBUSB))

all: debug

release debug: Android.mk config.h
	$(MY_BUILD) -C jni APP_OPTIM=$@ $(MY_OPTS) V=1

clean:
	$(RM) -r lib obj

